# Contributors
This module was primarily written by Richard Bowman, originally as part of ``nplab`` while he was at the University of Cambridge, and subsequently as part of this stand-alone module as part of the [OpenFlexure project] at the University of Bath.  If you contribute code to the project, please add your name to the list below, along with a brief description.

* Richard Bowman: main author and maintainer
* Joel Collins: updates to ``ofm_extension`` submodule
* Joe Knapper: ``scan_coords_times`` submodule

``array_with_attrs`` and ``camera_with_location`` may contain code from other ``nplab`` contributors, under the GPL.

[OpenFlexure project]: https://openflexure.org/